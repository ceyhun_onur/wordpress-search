
<?php
/**
 * The template for displaying Search Results pages
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty Fourteen 1.0
 */
get_header ();
$_key = trim ( $_GET ['s'] != '' ? $_GET ['s'] : '' ); // key to search
$_auth = trim ( $_GET ['auth'] != '' ? $_GET ['auth'] : '' ); // author to search
$_sd = $_GET ['sd'] != '' ? $_GET ['sd'] : 1; // selected start day to search
$_sm = $_GET ['sm'] != '' ? $_GET ['sm'] : 1; // selected start month to search
$_sy = $_GET ['sy'] != '' ? $_GET ['sy'] : 2000; // selected start year to search
$_fd = $_GET ['fd'] != '' ? $_GET ['fd'] : date ( 'd' ); // selected final day to search
$_fm = $_GET ['fm'] != '' ? $_GET ['fm'] : date ( 'm' ); // selected final month to search
$_fy = $_GET ['fy'] != '' ? $_GET ['fy'] : date ( 'Y' ); // selected final year to search

$_date_query = array (
		array (
				'after' => array (
						'day' => $_sd,
						'month' => $_sm,
						'year' => $_sy 
				),
				'before' => array (
						'day' => $_fd,
						'month' => $_fm,
						'year' => $_fy 
				),
				'inclusive' => true 
		) 
);
if (strpos ( $_key, '||' ) !== false) {
	$wp_query = new WP_Query ();
	$comments_query = new WP_Comment_Query ();
	$comments = array ();
	
	$_wordArray = explode ( '||', $_key );
	
	foreach ( $_wordArray as $i => $word ) {
		
		$word_query = new WP_Query ( array (
				's' => trim ( $_wordArray [$i] ),
				'date_query' => $_date_query 
		) );
		$word_comments = $comments_query->query ( array (
				'search' => trim ( $_wordArray [$i] ),
				'date_query' => $_date_query 
		) );
		if ($i == 0) {
			$wp_query = $word_query;
			$comments = $word_comments;
		} else {
			$wp_query->posts = array_unique ( array_merge ( $wp_query->posts, $word_query->posts ), SORT_REGULAR );
			$wp_query->post_count += $word_query->post_count;
			
			$comments = array_unique ( array_merge ( $comments, $word_comments ), SORT_REGULAR );
		}
	}
} else {
	
	$wp_query = new WP_Query ( array (
			's' => $_key,
			'date_query' => $_date_query 
	) );
	$comments_query = new WP_Comment_Query ();
	$comments = $comments_query->query ( array (
			'search' => $_key,
			'date_query' => $_date_query 
	) );
}
if ($_auth != '') {
	
	foreach ( $comments as $i => $comment ) {
		
		if (stripos ( $comment->comment_author, $_auth ) === false) {
			unset ( $comments [$i] );
		}
	}
}
?>

<section id="primary" class="content-area">
	<div id="content" class="site-content" role="main">

		<?php  	if (($comments || $wp_query->have_posts())) : ?>

		<header class="page-header">
			<h1 class="page-title"><?php printf( __( 'Search Results for: %s', 'twentyfourteen' ), $_key ); ?></h1>
			<h2 class="page-title"><?php if($_GET['auth'] != ''){ printf( __( 'Author Search Results for: %s', 'twentyfourteen' ), $_auth ); }?></h2>
			<h2 class="page-title"><?php if($_GET['sd'] != '' && $_GET['sm'] != '' && $_GET['sy'] != ''&& $_GET['fd'] != ''&& $_GET['fm'] != ''&& $_GET['fy'] != ''){ printf( __( 'Dates between: %s-%s-%s and %s-%s-%s', 'twentyfourteen' ), $_sd,$_sm,$_sy,$_fd,$_fm,	$_fy ); }?></h2>
		</header>
		<!-- .page-header -->

		<?php
			// Start the Loop.
			if ($wp_query->have_posts ()) {
				
				while ( $wp_query->have_posts () ) :
					$wp_query->the_post ();
					/*
					 * Include the post format-specific template for the content. If you want to
					 * use this in a child theme, then include a file called called content-___.php
					 * (where ___ is the post format) and that will be used instead.
					 */
					if ($_auth == '' || stripos ( get_the_author (), $_auth ) !== false) {
						get_template_part ( 'content', get_post_format () );
					}
				endwhile
				;
			}
			
			echo "<hr>";
			
			wp_list_comments ( array (
					'style' => 'ol',
					'short_ping' => true,
					'avatar_size' => 34,
					'end-callback' => 'hr' 
			), $comments );
			// Previous/next post navigation.
			twentyfourteen_paging_nav ();
		 

		else :
			// If no content, include the "No posts found" template.
			get_template_part ( 'content', 'none' );
		

		endif;
		?>

				</div>
	<!-- #content -->
</section>
<!-- #primary -->



<?php
get_sidebar ( 'content' );
get_sidebar ();
get_footer ();
