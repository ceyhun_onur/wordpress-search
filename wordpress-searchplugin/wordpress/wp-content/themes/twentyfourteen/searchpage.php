
<?php
/**
 * The template for displaying pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages and that
 * other "pages" on your WordPress site will use a different template.
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty Fourteen 1.0
 */

/*
 * Template Name: Advanced Search Page
 */
get_header ();
?>
<section id="primary" class="content-area">
	<div id="content" class="site-content" role="main">

		<form role="search" method="get" id="searchform" class="searchform"
			action="<?php echo site_url('/'); ?>">
			<div>
				<label class="screen-reader-text" for="s">Search for:</label> <label
					for="search">Search for:</label> <input type="text"
					label="Search for: "
					value="<?php echo $_GET['s'] != '' ? $_GET['s'] : ''; ?>"
					placeholder="Search for..." name="s" id="s" required /> <br></br> <label
					for="auth">Author name(optional):</label> <input type="text"
					label="Author Name(optional): "
					value="<?php echo $_GET['auth'] != '' ? $_GET['auth'] : ''; ?>"
					placeholder="Author Name(optional)" name="auth" id="auth" /> <br></br>
				<label>Select dates below(optional): </label> <br></br> <label>From:</label>
				<select name="sd">
					<option value="" selected="selected">Select a Day</option>
        <?php
								for($d = 1; $d <= 31; $d ++) {
									?> 
         	<option value="<?php echo $d; ?>"> 
         		<?php echo $d; ?>
         		<?php } ?>  </option>
				</select> <select name="sm">
					<option value="" selected="selected">Select a Month</option>
         <?php
									for($x = 1; $x <= 12; $x ++) {
										?>
          <option value="<?php echo $x; ?>">
          <?php echo $x; ?>
         <?php } ?>  </option>
				</select> <select name="sy">
					<option value="" selected="selected">Select a Year</option>
         <?php
									for($x = 2000; $x <= date ( 'Y' ); $x ++) {
										?>
          <option value="<?php echo $x; ?>">
          <?php echo $x; ?>
         <?php } ?>  </option>
				</select> <br></br> <label>To:</label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				<select name="fd">
					<option value="" selected="selected">Select a Day</option>
        <?php
								for($d = 1; $d <= 31; $d ++) {
									?> 
         	<option value="<?php echo $d; ?>"> 
         		<?php echo $d; ?>
         		<?php } ?>  </option>
				</select> <select name="fm">
					<option value="" selected="selected">Select a Month</option>
         <?php
									for($x = 1; $x <= 12; $x ++) {
										?>
          <option value="<?php echo $x; ?>">
          <?php echo $x; ?>
         <?php } ?>  </option>
				</select> <select name="fy">
					<option value="" selected="selected">Select a Year</option>
         <?php
									for($x = 2000; $x <= date ( 'Y' ); $x ++) {
										?>
          <option value="<?php echo $x; ?>">
          <?php echo $x; ?>
         <?php } ?>  </option>
				</select> <br></br> <input type="submit" id="searchsubmit"
					value="Search" />
			</div>
		</form>

	</div>
</section>






<?php
get_sidebar ( 'content' );
get_sidebar();
get_footer();
?>


