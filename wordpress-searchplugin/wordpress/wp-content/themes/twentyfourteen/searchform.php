<form role="search" method="get" id="searchform" class="searchform"
	action="<?php echo site_url('/'); ?>">
	<div>
		<label class="screen-reader-text" for="s">Search for:</label> <label
			for="search">Search for:</label> <input type="text"
			label="Search for: "
			value="<?php echo $_GET['s'] != '' ? $_GET['s'] : ''; ?>"
			placeholder="Search for..." name="s" id="s" required /> <input
			type="submit" id="searchsubmit" value="Search" />
	</div>
</form>